#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <microhttpd.h>
#include <cJSON.h>

static enum MHD_Result valueEnumerator(void *cls,
                                        enum MHD_ValueKind kind,
                                        const char *key,
                                        const char *value) {
    cJSON* headers = (cJSON*)cls ;
    cJSON_AddStringToObject(headers, key, value) ;
    return MHD_YES ;
}

static enum MHD_Result enumerateValues(struct MHD_Connection* connection,
    enum MHD_ValueKind kind,
    cJSON* root) {
    return  MHD_get_connection_values(connection, kind, valueEnumerator, root) ;
}

static struct MHD_Response* createResponseFromJSON(cJSON* root) {
    char* buffer = cJSON_Print(root) ;
    struct MHD_Response* response = NULL ;

    if (buffer) {
        if ((response = MHD_create_response_from_buffer(strlen(buffer),
                                    (void*)buffer,
                                    MHD_RESPMEM_MUST_FREE)) != NULL) {
            MHD_add_response_header(response, MHD_HTTP_HEADER_CONTENT_TYPE, "application/json") ;
        } else {
            free(buffer) ;
        }
    }

    return response ;
}

static enum MHD_Result
ahc_echo(void * cls,
         struct MHD_Connection * connection,
         const char * url,
         const char * method,
         const char * version,
         const char * upload_data,
         size_t * upload_data_size,
         void ** ptr) {
    char* hostname = (char*)alloca(64);
    static int dummy;
    struct MHD_Response * response;
    int ret;
    int status = MHD_HTTP_OK ;

    fprintf(stderr, "%s %s %s *ptr=%p\n", method, url, version, *ptr) ;

    if (0 != strcmp(method, "GET")) {
        return MHD_NO; /* unexpected method */
    }
    if (&dummy != *ptr) {
        *ptr = &dummy;
        return MHD_YES;
    }
    if (0 != *upload_data_size) {
        return MHD_NO; /* upload data in a GET!? */
    }

    *ptr = NULL; /* clear context pointer */

    int r = gethostname(hostname, 64) ;

    cJSON* root = cJSON_CreateObject() ;

    cJSON* headers = cJSON_CreateObject() ;
    cJSON* parameters = cJSON_CreateObject() ;
    cJSON* cookies = cJSON_CreateObject() ;

    enumerateValues(connection, MHD_HEADER_KIND, headers) ;
    enumerateValues(connection, MHD_GET_ARGUMENT_KIND, parameters) ;
    enumerateValues(connection, MHD_COOKIE_KIND, cookies) ;

    cJSON_AddStringToObject(root, "hostname", hostname) ;
    cJSON_AddItemToObject(root, "headers", headers) ;
    cJSON_AddItemToObject(root, "parameters", parameters) ;
    cJSON_AddItemToObject(root, "cookies", cookies) ;
    cJSON_AddStringToObject(root, "method", method) ;
    cJSON_AddStringToObject(root, "url", url) ;
    cJSON_AddStringToObject(root, "version", version) ;

    response = createResponseFromJSON(root) ;

    cJSON* item ;
    if ((item = cJSON_GetObjectItem(cookies, "session")) == NULL) {
        MHD_add_response_header(response, MHD_HTTP_HEADER_SET_COOKIE, "session=1234567897") ;
    } else {
        fprintf(stderr, "cookie = %s\n", item->valuestring) ;
    }

    cJSON_Delete(root) ;

    ret = MHD_queue_response(connection,
			                 status,
			                 response) ;
    MHD_destroy_response(response) ;
    
    return ret;
}

static int signalled = 0;

void sig_handler(int signo) {
    fprintf(stderr, "Received signal %d\n", signo) ;
    signalled = 1 ;
}

int main(int argc, const char** argv) {
    struct MHD_Daemon* daemon ;

    signal(SIGINT, sig_handler) ;

    daemon = MHD_start_daemon(MHD_USE_THREAD_PER_CONNECTION,
		       8080,
		       NULL, NULL,
		       &ahc_echo, NULL,
		       MHD_OPTION_END) ;

    while(!signalled) {
        sleep(1) ;
    }

    MHD_stop_daemon(daemon) ;

    return 0 ;
}
 