FROM alpine:latest as buildStage

RUN apk update && apk add cmake make g++ binutils libtool automake git
WORKDIR /building
COPY . .

ENV CMAKE_BUILD_TYPE=MinSizeRel
ENV DOCKER_BUILD=YES 
RUN mkdir build && cd build && cmake -DBUILD_SHARED_LIBS=OFF -DDOCKER_BUILD=${DOCKER_BUILD} .. 
RUN cd build && cmake --build . --target all --
RUN ls -l build/
RUN cd build/src && strip minimal

FROM scratch

EXPOSE 8080/tcp

WORKDIR /app
COPY --from=buildStage /building/build/src/minimal .

ENTRYPOINT [ "/app/minimal" ] 
